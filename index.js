const { request, Router } = require("express");
const express = require("express");

const app = express();

const courseRouter = require("./app/routes/courseRouter")
const reviewRouter = require("./app/routes/reviewRouter")

const port = 8000;
app.use(function(request, response, next){
    console.log("Current time: ", new Date());
    next();
})
app.use(function(request, response, next) {
    console.log("Request method: ", request.method);
    next();
})

app.use("/api", courseRouter);
app.use("/api", reviewRouter);

app.get("/", function(request, response) {
    response.json({
        message: "Devcamp Middleware Express APP"
    })
})

app.listen(port, function() {
    console.log("App running on port 8000");
})