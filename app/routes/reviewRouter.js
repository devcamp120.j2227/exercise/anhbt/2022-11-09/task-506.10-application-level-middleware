const express = require("express");

const router = express.Router();

const reviewMiddleware = require("../middleware/reviewMiddleware");

router.use(function(request, response, next) {
    console.log(`Request URL: ${request.url}`);
    next();
})

router.get("/reviews", reviewMiddleware.getAllReviewMiddleware, function(request, response){
    response.json({
        message: 'Get ALL Reviews'
    })
})
router.post("/reviews", reviewMiddleware.createReviewMiddleware, function(request, response){
    response.json({
        message: 'Create Review'
    })
})
router.get("/reviews/:reviewid", reviewMiddleware.getDetailReviewMiddleware , function(request, response){
    const reviewId = request.params.reviewId
    response.json({
        message: `Get Review with ID ${reviewId}`
    })
})
router.put("/reviews/:reviewid", reviewMiddleware.updateReviewMiddleware, function(request, response){
    const reviewId = request.params.reviewId
    response.json({
        message: `Update Review with ID ${reviewId}`
    })
})
router.delete("/reviews/:reviewid", reviewMiddleware.deleteReviewMiddleware, function(request, response){
    const reviewId = request.params.reviewId
    response.json({
        message: `Delete Review with ID ${reviewId}`
    })
})

module.exports = router;