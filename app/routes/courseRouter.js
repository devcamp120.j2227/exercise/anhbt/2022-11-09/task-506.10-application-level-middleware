const express = require("express");

const router = express.Router();

const courseMiddleware = require("../middleware/courseMiddleware");

router.use(function(request, response, next) {
    console.log(`Request URL: ${request.url}`);
    next();
})

router.get("/courses", courseMiddleware.getAllCourseMiddleware, function(request, response){
    response.json({
        message: 'Get ALL Courses'
    })
})
router.post("/courses", courseMiddleware.createCourseMiddleware, function(request, response){
    response.json({
        message: 'Create Course'
    })
})
router.get("/courses/:courseid", courseMiddleware.getDetailCourseMiddleware , function(request, response){
    const courseId = request.params.courseId
    response.json({
        message: `Get Course with ID ${courseId}`
    })
})
router.put("/courses/:courseid", courseMiddleware.updateCourseMiddleware, function(request, response){
    const courseId = request.params.courseId
    response.json({
        message: `Update Course with ID ${courseId}`
    })
})
router.delete("/courses/:courseid", courseMiddleware.deleteCourseMiddleware, function(request, response){
    const courseId = request.params.courseId
    response.json({
        message: `Delete Course with ID ${courseId}`
    })
})

module.exports = router;