const getAllReviewMiddleware = function(request, response, next) {
    console.log(`get ALL review middleware`);
    next();
}

const createReviewMiddleware = function(request, response, next) {
    console.log(`create review middleware`);
    next();
}

const getDetailReviewMiddleware = function(request, response, next) {
    console.log(`get detail review middleware`);
    next();
}

const updateReviewMiddleware = function(request, response, next) {
    console.log(`update review middleware`);
    next();
}

const deleteReviewMiddleware = function(request, response, next) {
    console.log(`delete review middleware`);
    next();
}

module.exports = {
    getAllReviewMiddleware,
    createReviewMiddleware,
    getDetailReviewMiddleware,
    getAllReviewMiddleware,
    updateReviewMiddleware,
    deleteReviewMiddleware
}