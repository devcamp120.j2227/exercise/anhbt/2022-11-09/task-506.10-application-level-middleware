const getAllCourseMiddleware = function(request, response, next) {
    console.log(`get ALL course middleware`);
    next();
}

const createCourseMiddleware = function(request, response, next) {
    console.log(`create course middleware`);
    next();
}

const getDetailCourseMiddleware = function(request, response, next) {
    console.log(`get detail course middleware`);
    next();
}

const updateCourseMiddleware = function(request, response, next) {
    console.log(`update course middleware`);
    next();
}

const deleteCourseMiddleware = function(request, response, next) {
    console.log(`delete course middleware`);
    next();
}

module.exports = {
    getAllCourseMiddleware,
    createCourseMiddleware,
    getDetailCourseMiddleware,
    getAllCourseMiddleware,
    updateCourseMiddleware,
    deleteCourseMiddleware
}